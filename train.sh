#!/bin/bash

#args are : output folder, number of categories, directory with CASIA-WebFace structure, file with labels, optionally -s to use face segmentation 

python3 train_model.py model 10 data names_10.txt
python3 train_model.py model 10 data names_10.txt -s
