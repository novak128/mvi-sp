# Face Recognition - porovnání výsledků s využítím segmentace a bez ní

Nátrenujte metody z `face_detector.py` k detekci obličejů a natrénujte vlastní nebo použijte předtrénovaný model se segmentací obličejové oblasti nebo bez ní k rozpoznávání obličejů.

# Spuštění
Pro spuštění je nutné naklonovat repozitář https://github.com/shaoanlu/face_toolbox_keras pomocí `git clone https://github.com/shaoanlu/face_toolbox_keras.git`

Natrénovat vlastní model můžete například pomocí skriptu `train.sh` (vysvětlivky v komentáři ve skriptu)

Vyzkoušet natrénované modely můžete v připraveném jupiter notebooku `test_run.ipynb`


