import numpy as np
import cv2
from face_toolbox_keras.models.parser import face_parser

cascade_face = "haarcascade_frontalface_default.xml"

FACE_SIZE = 96

PARSER = face_parser.FaceParser()

def get_faces(img, useSegmentation):
    coords = detect_face(img)
    faces = normalize_faces(img, coords)
    if useSegmentation == True:
        faces = segmentation(faces)
    return coords, faces

def get_faces_from_img_path(image_path, useSegmentation = False):
    return get_faces(cv2.imread(image_path, 1), useSegmentation)

def detect_face(image):
    classifier = cv2.CascadeClassifier(cascade_face)
    faces_coord = classifier.detectMultiScale(
        image,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    return faces_coord

def normalize_faces(image, faces_coords):
    faces = []

    for i in range(0, len(faces_coords)):
        x, y, w, h = faces_coords[i]
        faces.append(image[y:y+h, x:x+w])
    
    size = (FACE_SIZE, FACE_SIZE)
    result = []
    for face in faces:
        if face.shape < size:
            try:
                image_norm = cv2.resize(face, size, interpolation=cv2.INTER_AREA)
                result.append(image_norm)
            except Exception:
                pass
        else:
            try:
                image_norm = cv2.resize(face, size, interpolation=cv2.INTER_CUBIC)
                result.append(image_norm)
            except Exception:
                pass
    return result

def segmentation(faces):
    result = []
    for face in faces:
        out = PARSER.parse_face(face)
        face_segment = face.copy()
        for i in range(0, len(out[0])):
            for j in range(0, len(out[0][i])):
                if out[0][i][j] < 14 and out[0][i][j] > 0:
                    continue
                else:
                    face_segment[i][j] = [0,0,0]
        result.append(face_segment)
    return result

