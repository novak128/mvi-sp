import os
import sys
import numpy as np
import csv
import tensorflow as tf
import cv2
import math
from pathlib import Path
from os import listdir
from os.path import isfile, join
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.losses import sparse_categorical_crossentropy
from keras.optimizers import Adam
from keras.regularizers import l2
import face_detector as fd

#arguments
arguments = len(sys.argv) - 1

if arguments != 4 and arguments != 5:
    print("possible usage: python3 train_model.py output 5 data5 names_5.txt [-s] optional for segmentation")
    sys.exit()

MODEL_OUT_DIR = sys.argv[1]
if arguments == 5 and sys.argv[5] == "-s":
    SEGMENTATION = True;
else:
    SEGMENTATION = False;

######CONFIGURATION

DATA_FOLDER = sys.argv[3]
LABELS = sys.argv[4]
IMG_SIZE = 250
FACE_SIZE = 96

NUM_FEATS = 64
BATCH_SIZE = 128
EPOCHS = 50

NUM_CATEGS = int(sys.argv[2])
epoch_path = MODEL_OUT_DIR + "/output/{epoch:03d}.epoch"

print("configuration set up with SEGMENTATION as ", SEGMENTATION)
#return a dictionary that links image folders to people
def get_labels():
    dataset_labels = {}
    with open(LABELS) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            dataset_labels[row[0]] = row[2]
    return dataset_labels

def divide_into_train_test_validation():
    dataset_labels = get_labels()
    folder_list = list(dataset_labels.keys())
    for dirs in folder_list:
        current_path = DATA_FOLDER + "/" + dirs + "/"
        onlyfiles = [f for f in listdir(current_path) if isfile(join(current_path, f))]
        if not os.path.exists(current_path + 'train'):
            os.mkdir(current_path + 'train')
        if not os.path.exists(current_path + 'test'):
            os.mkdir(current_path + 'test')
        if not os.path.exists(current_path + 'validation'):
            os.mkdir(current_path + 'validation')

        train_limit = math.ceil(len(onlyfiles)*0.75)
        test_limit = math.ceil((len(onlyfiles)-train_limit)*0.5)
        for i in range(0, len(onlyfiles)):
            if i < train_limit:
                Path(current_path + onlyfiles[i]).rename(current_path + "train/" + onlyfiles[i])
            elif i < train_limit + test_limit:
                Path(current_path + onlyfiles[i]).rename(current_path + "test/" + onlyfiles[i])
            else:
                Path(current_path + onlyfiles[i]).rename(current_path + "validation/" + onlyfiles[i])

def load_images(subdir, segmentation = False):
    dataset_labels = get_labels()
    folder_list = list(dataset_labels.keys())
    images = []
    labels = []
    for dir in folder_list:
        current_path = DATA_FOLDER + "/" + dir + "/" + subdir + "/"
        onlyfiles = [f for f in listdir(current_path) if isfile(join(current_path, f))]
        for i in range(0, len(onlyfiles)):
            coords, faces = fd.get_faces_from_img_path(current_path + onlyfiles[i], segmentation)
            for j in range(0, len(faces)):
                images.append(faces[j])
                labels.append(dataset_labels[dir])
    return images, labels

print("preparing data...")

divide_into_train_test_validation()

print("loading data and croping faces...")

train_data,train_labels = load_images("train", segmentation=SEGMENTATION)
print("train data loaded...")
test_data, test_labels = load_images("test", segmentation=SEGMENTATION)
print("test data loaded...")
val_data, val_labels = load_images("validation", segmentation=SEGMENTATION)
print("validation data loaded...")

print("Train size:", len(train_data), ", Test size:", len(test_data), ", Validation size:", len(val_data))

mean = np.mean(train_data, axis = 0)
std = np.std(train_data, axis = 0)

train_data = (train_data - mean) / std
test_data = (test_data - mean) / std
val_data = (val_data - mean) / std

if not os.path.exists(MODEL_OUT_DIR + '/output'):
    os.mkdir(MODEL_OUT_DIR + '/output')

model = Sequential([
    Conv2D(NUM_FEATS, kernel_size=(3, 3), activation='relu', input_shape=(FACE_SIZE, FACE_SIZE, 3), data_format='channels_last', kernel_regularizer=l2(0.01)),
    Conv2D(NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Conv2D(2*NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    Conv2D(2*NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Conv2D(4*NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    Conv2D(4*NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Conv2D(8*NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    Conv2D(8*NUM_FEATS, kernel_size=(3, 3), activation='relu', padding='same'),
    BatchNormalization(),
    MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    Dropout(0.5),

    Flatten(),

    Dense(8*NUM_FEATS, activation='relu'),
    Dropout(0.4),
    Dense(4*NUM_FEATS, activation='relu'),
    Dropout(0.4),
    Dense(2*NUM_FEATS, activation='relu'),
    Dropout(0.5),

    Dense(NUM_CATEGS, activation='softmax')
])

print("Compiling model...")

model.compile(
    loss=sparse_categorical_crossentropy,
    optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7),
    metrics=['accuracy'])

print("Saving model...")

model.save(MODEL_OUT_DIR + "/model.out")

epoch_callbacks = tf.keras.callbacks.ModelCheckpoint(
    filepath=epoch_path,
    verbose=1,
    save_weights_only=True,
    period=1
)

model.save_weights(epoch_path.format(epoch=0))

print("Running model.fit...")

model.fit(
    np.array(train_data), np.array(train_labels),
    batch_size=BATCH_SIZE,
    epochs=EPOCHS,
    verbose=1,
    validation_data=(np.array(val_data), np.array(val_labels)),
    shuffle=True,
    callbacks=[epoch_callbacks]
)
